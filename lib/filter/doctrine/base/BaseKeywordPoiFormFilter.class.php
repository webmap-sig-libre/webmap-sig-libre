<?php

/**
 * KeywordPoi filter form base class.
 *
 * @package    webmap-sig-libre
 * @subpackage filter
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseKeywordPoiFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
    ));

    $this->setValidators(array(
    ));

    $this->widgetSchema->setNameFormat('keyword_poi_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'KeywordPoi';
  }

  public function getFields()
  {
    return array(
      'poi_id'     => 'Number',
      'keyword_id' => 'Number',
    );
  }
}
