<?php

/**
 * Keyword filter form base class.
 *
 * @package    webmap-sig-libre
 * @subpackage filter
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseKeywordFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'logo'      => new sfWidgetFormFilterInput(),
      'pois_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Poi')),
    ));

    $this->setValidators(array(
      'logo'      => new sfValidatorPass(array('required' => false)),
      'pois_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Poi', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('keyword_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addPoisListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.KeywordPoi KeywordPoi')
      ->andWhereIn('KeywordPoi.poi_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Keyword';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'logo'      => 'Text',
      'pois_list' => 'ManyKey',
    );
  }
}
