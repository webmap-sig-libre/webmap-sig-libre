<?php

/**
 * Poi filter form base class.
 *
 * @package    webmap-sig-libre
 * @subpackage filter
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePoiFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'     => new sfWidgetFormFilterInput(),
      'address'         => new sfWidgetFormFilterInput(),
      'postal_code'     => new sfWidgetFormFilterInput(),
      'city'            => new sfWidgetFormFilterInput(),
      'country'         => new sfWidgetFormFilterInput(),
      'sub_category_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SubCategory'), 'add_empty' => true)),
      'phone'           => new sfWidgetFormFilterInput(),
      'website'         => new sfWidgetFormFilterInput(),
      'email'           => new sfWidgetFormFilterInput(),
      'longitude'       => new sfWidgetFormFilterInput(),
      'latitude'        => new sfWidgetFormFilterInput(),
      'photo'           => new sfWidgetFormFilterInput(),
      'is_published'    => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'slug'            => new sfWidgetFormFilterInput(),
      'keyword_list'    => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Keyword')),
    ));

    $this->setValidators(array(
      'name'            => new sfValidatorPass(array('required' => false)),
      'description'     => new sfValidatorPass(array('required' => false)),
      'address'         => new sfValidatorPass(array('required' => false)),
      'postal_code'     => new sfValidatorPass(array('required' => false)),
      'city'            => new sfValidatorPass(array('required' => false)),
      'country'         => new sfValidatorPass(array('required' => false)),
      'sub_category_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SubCategory'), 'column' => 'id')),
      'phone'           => new sfValidatorPass(array('required' => false)),
      'website'         => new sfValidatorPass(array('required' => false)),
      'email'           => new sfValidatorPass(array('required' => false)),
      'longitude'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'latitude'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'photo'           => new sfValidatorPass(array('required' => false)),
      'is_published'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'slug'            => new sfValidatorPass(array('required' => false)),
      'keyword_list'    => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Keyword', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('poi_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addKeywordListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.KeywordPoi KeywordPoi')
      ->andWhereIn('KeywordPoi.keyword_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Poi';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'name'            => 'Text',
      'description'     => 'Text',
      'address'         => 'Text',
      'postal_code'     => 'Text',
      'city'            => 'Text',
      'country'         => 'Text',
      'sub_category_id' => 'ForeignKey',
      'phone'           => 'Text',
      'website'         => 'Text',
      'email'           => 'Text',
      'longitude'       => 'Number',
      'latitude'        => 'Number',
      'photo'           => 'Text',
      'is_published'    => 'Boolean',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
      'slug'            => 'Text',
      'keyword_list'    => 'ManyKey',
    );
  }
}
