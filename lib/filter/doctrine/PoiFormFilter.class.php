<?php

/**
 * Poi filter form.
 *
 * @package    webmap-sig-libre
 * @subpackage filter
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PoiFormFilter extends BasePoiFormFilter
{
  public function configure()
  {
    $this->widgetSchema['sub_category_id']->addOption('order_by',array('name','asc'));

  }
}
