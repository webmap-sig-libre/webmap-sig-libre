<?php

/**
 * KeywordTranslation filter form.
 *
 * @package    webmap-sig-libre
 * @subpackage filter
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class KeywordTranslationFormFilter extends BaseKeywordTranslationFormFilter {

  public function configure() {
    
  }

}