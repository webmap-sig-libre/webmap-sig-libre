<?php

/**
 * Club filter form.
 *
 * @package    webmap-sig-libre
 * @subpackage filter
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FrontPoiFormFilter extends PoiFormFilter {

  public function configure() {
    
    parent::setQuery(Doctrine::getTable('Poi')->getPublishedPois());

    parent::configure();
    $this->useFields(array('sub_category_id'));
    
    $this->widgetSchema['sub_category_id']->addOption('multiple', true);
    $this->widgetSchema['sub_category_id']->addOption('add_empty', false);
    
    $this->validatorSchema['sub_category_id']->addOption('multiple', true);

    $this->widgetSchema['category_id'] = new sfWidgetFormDoctrineChoice(array(
                'model' => 'Category',
                'query' => Doctrine::getTable('Category')->createQuery('q'),
                "order_by" => array('name', 'asc'),
                'multiple' => false,
                'add_empty' => ''
            ));    
    
    //$this->widgetSchema->setHelp('category', '(Sélection multiple avec<br/>les touches Ctrl ou Maj)');
    

    $this->validatorSchema['category_id'] = new sfValidatorDoctrineChoice(
                    array(
                        'model' => 'Category',
                        'query' => Doctrine::getTable('Category')->createQuery('q'),
                        'required' => false
            ));

    $this->widgetSchema->setLabels(array(
        'category_id' => 'Category'
    ));
  }

  public function getFields() {
    return array_merge(parent::getFields(), array('category_id' => 'Text'));
  }

  public function addCategoryIdColumnQuery(Doctrine_Query $query, $field, $value) {
    $fieldName = $this->getFieldName($field);

    if ($value) {
      $rootAlias = $query->getRootAlias();
      $query->innerJoin($rootAlias . '.SubCategory sub');
      $query->innerJoin('sub.Category c');
      $query->andWhereIn('sub.category_id', $value);
    }

    return $query;
  }

}
