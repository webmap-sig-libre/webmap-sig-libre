<?php

/**
 * BaseSubCategory
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $name
 * @property clob $description
 * @property string $logo
 * @property integer $category_id
 * @property Category $Category
 * @property Doctrine_Collection $Pois
 * 
 * @method integer             getId()          Returns the current record's "id" value
 * @method string              getName()        Returns the current record's "name" value
 * @method clob                getDescription() Returns the current record's "description" value
 * @method string              getLogo()        Returns the current record's "logo" value
 * @method integer             getCategoryId()  Returns the current record's "category_id" value
 * @method Category            getCategory()    Returns the current record's "Category" value
 * @method Doctrine_Collection getPois()        Returns the current record's "Pois" collection
 * @method SubCategory         setId()          Sets the current record's "id" value
 * @method SubCategory         setName()        Sets the current record's "name" value
 * @method SubCategory         setDescription() Sets the current record's "description" value
 * @method SubCategory         setLogo()        Sets the current record's "logo" value
 * @method SubCategory         setCategoryId()  Sets the current record's "category_id" value
 * @method SubCategory         setCategory()    Sets the current record's "Category" value
 * @method SubCategory         setPois()        Sets the current record's "Pois" collection
 * 
 * @package    webmap-sig-libre
 * @subpackage model
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseSubCategory extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('sub_category');
        $this->hasColumn('id', 'integer', 6, array(
             'type' => 'integer',
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => 6,
             ));
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'unique' => true,
             'length' => 100,
             ));
        $this->hasColumn('description', 'clob', null, array(
             'type' => 'clob',
             ));
        $this->hasColumn('logo', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('category_id', 'integer', 6, array(
             'type' => 'integer',
             'unsigned' => true,
             'notnull' => true,
             'length' => 6,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Category', array(
             'local' => 'category_id',
             'foreign' => 'id'));

        $this->hasMany('Poi as Pois', array(
             'local' => 'id',
             'foreign' => 'sub_category_id'));
    }
}