<?php

/**
 * Category form.
 *
 * @package    webmap-sig-libre
 * @subpackage form
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CategoryForm extends BaseCategoryForm
{
  public function configure()
  {
    /* Name */
    $this->validatorSchema['name'] = new sfValidatorString(
                    array(
                        'required' => true,
                        'min_length' => 2,
                        'max_length' => 50
                    ),
                    array(
                        'required' => 'Please give a name',
                        'min_length' => '"%value%" looks to be too short: %min_length% characters min',
                        'max_length' => '"%value%" looks to be a bit too long: %max_length% characters max'
                    )
    );
    
    $this->validatorSchema->setPostValidator(new sfValidatorDoctrineUnique(
                    array('model' => 'Category', 'column' => 'name'),
                    array('invalid' => 'This name already exists')
            )
    );
  }
}
