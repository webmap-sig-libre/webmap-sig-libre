<?php

/**
 * SubCategory form.
 *
 * @package    webmap-sig-libre
 * @subpackage form
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SubCategoryForm extends BaseSubCategoryForm {

  public function configure() {
    /* Name */
    $this->validatorSchema['name'] = new sfValidatorString(
                    array(
                        'required' => true,
                        'min_length' => 2,
                        'max_length' => 50
                    ),
                    array(
                        'required' => 'The name is mandatory.',
                        'min_length' => '"%value%" is too short (2-50 chars).',
                        'max_length' => '"%value%" is too long (2-50 chars).'
                    )
    );

    /* Logo */
    $this->widgetSchema['logo'] = new sfWidgetFormInputFileEditable(array(
                'label' => 'Logo',
                'file_src' => '/uploads/logos/' . $this->getObject()->getLogo(),
                'is_image' => true
            ));

    $this->validatorSchema['logo'] = new sfValidatorFile(
                    array(
                        'required' => $this->isNew(),
                        'path' => sfConfig::get('sf_upload_dir') . '/logos',
                        'max_size' => 2048000,
                        'mime_types' => 'web_images'
                    ),
                    array(
                        'required' => 'Don\'t forget the logo.',
                        'max_size' => 'File too large (2MB max)',
                        'mime_types' => 'The logo must be a web image file (png, jpg or gif)'
                    )
    );

    $this->validatorSchema['logo_delete'] = new sfValidatorPass();
    
    $this->validatorSchema->setPostValidator(new sfValidatorDoctrineUnique(
                    array('model' => 'SubCategory', 'column' => 'name'),
                    array('invalid' => 'This name already exists')
            )
    );
  }

}
