<?php

/**
 * Poi form.
 *
 * @package    webmap-sig-libre
 * @subpackage form
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PoiForm extends BasePoiForm
{
  public function configure()
  {
    $this->removeFields();
    
    $this->widgetSchema['name']->setAttribute('size', 50);
    $this->validatorSchema['name'] = new sfValidatorString(
                    array(
                        'required' => true,
                        'min_length' => 2,
                        'max_length' => 100
                    ),
                    array(
                        'required' => 'Please give a name',
                        'min_length' => '"%value%" looks to be too short: %min_length% characters min',
                        'max_length' => '"%value%" looks to be a bit too long: %max_length% characters max'
                    )
    );
    
    /* country  */
    $this->validatorSchema['country'] = new sfValidatorString(
                    array(
                        'required' => FALSE,
                        'min_length' => 2,
                        'max_length' => 100
                    ),
                    array(
                        'min_length' => '"%value%" looks to be too short: %min_length% characters min',
                        'max_length' => '"%value%" looks to be a bit too long: %max_length% characters max'
                    )
    );
    
    /* web : contrôle URL http ou https */
    $this->widgetSchema['website']->setAttribute('size', 50);
    $this->validatorSchema['website'] = new sfValidatorUrl(
                    array(
                        'required' => false
                    ),
                    array(
                        'invalid' => 'The website address should be in the form: http://...'
                    )
    );

    /* longitude */
    $this->widgetSchema['longitude']->setAttribute('size', 10);
    $this->validatorSchema['longitude'] = new sfValidatorNumber(
                    array(
                        'required' => false,
                        'min' => -180,
                        'max' => 180
                    ),
                    array(
                        'invalid' => '"%value%" is not a number',
                        'min' => 'Longitude is between -180 and 180',
                        'max' => 'Longitude is between -180 and 180'
                    )
    );

    /* latitude */
    $this->widgetSchema['latitude']->setAttribute('size', 10);
    $this->validatorSchema['latitude'] = new sfValidatorNumber(
                    array(
                        'required' => false,
                        'min' => -90,
                        'max' => 90
                    ),
                    array(
                        'invalid' => '"%value%" is not a number',
                        'min' => 'Latitude  is between -90 and 90',
                        'max' => 'Latitude  is between -90 and 90'
                    )
    );
    
    // email
    $this->validatorSchema['email'] = new sfValidatorEmail(
            array('required' => false),
            array('invalid' => 'Please enter a valid email address')
            );
    
    /* Photo */
    $this->widgetSchema['photo'] = new sfWidgetFormInputFileEditable(array(
                'label' => 'Photo',
                'file_src' => '/uploads/photos/source/' . $this->getObject()->getPhoto(),
                'is_image' => true
            ));

    $this->validatorSchema['photo'] = new sfValidatorFile(
                    array(
                        'required' => false,
                        'path' => sfConfig::get('sf_upload_dir') . '/photos/source',
                        'max_size' => 2048000,
                        'mime_types' => 'web_images'
                    ),
                    array(
                        'max_size' => 'File too large (2MB max)',
                        'mime_types' => 'The photo must be a web image file (png, jpg or gif)'
                    )
    );

    $this->validatorSchema['photo_delete'] = new sfValidatorPass();
    
    $this->validatorSchema->setPostValidator(new sfValidatorDoctrineUnique(
                    array('model' => 'Poi', 'column' => 'name'),
                    array('invalid' => 'This name already exists')
            )
    );
  }
  
  protected function removeFields() {
    unset(
            $this['created_at'], $this['updated_at'], $this['slug']
    );
  }
}
