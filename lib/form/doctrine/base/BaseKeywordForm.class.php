<?php

/**
 * Keyword form base class.
 *
 * @method Keyword getObject() Returns the current form's model object
 *
 * @package    webmap-sig-libre
 * @subpackage form
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseKeywordForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'logo'      => new sfWidgetFormInputText(),
      'pois_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Poi')),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'logo'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'pois_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Poi', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('keyword[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Keyword';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['pois_list']))
    {
      $this->setDefault('pois_list', $this->object->Pois->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->savePoisList($con);

    parent::doSave($con);
  }

  public function savePoisList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['pois_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Pois->getPrimaryKeys();
    $values = $this->getValue('pois_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Pois', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Pois', array_values($link));
    }
  }

}
