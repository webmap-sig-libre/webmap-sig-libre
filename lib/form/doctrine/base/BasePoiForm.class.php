<?php

/**
 * Poi form base class.
 *
 * @method Poi getObject() Returns the current form's model object
 *
 * @package    webmap-sig-libre
 * @subpackage form
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePoiForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'name'            => new sfWidgetFormInputText(),
      'description'     => new sfWidgetFormTextarea(),
      'address'         => new sfWidgetFormTextarea(),
      'postal_code'     => new sfWidgetFormInputText(),
      'city'            => new sfWidgetFormInputText(),
      'country'         => new sfWidgetFormInputText(),
      'sub_category_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SubCategory'), 'add_empty' => false)),
      'phone'           => new sfWidgetFormInputText(),
      'website'         => new sfWidgetFormInputText(),
      'email'           => new sfWidgetFormInputText(),
      'longitude'       => new sfWidgetFormInputText(),
      'latitude'        => new sfWidgetFormInputText(),
      'photo'           => new sfWidgetFormInputText(),
      'is_published'    => new sfWidgetFormInputCheckbox(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'slug'            => new sfWidgetFormInputText(),
      'keyword_list'    => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Keyword')),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'            => new sfValidatorString(array('max_length' => 100)),
      'description'     => new sfValidatorString(array('required' => false)),
      'address'         => new sfValidatorString(array('required' => false)),
      'postal_code'     => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'city'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'country'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sub_category_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SubCategory'))),
      'phone'           => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'website'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'longitude'       => new sfValidatorNumber(array('required' => false)),
      'latitude'        => new sfValidatorNumber(array('required' => false)),
      'photo'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'is_published'    => new sfValidatorBoolean(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'slug'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'keyword_list'    => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Keyword', 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorDoctrineUnique(array('model' => 'Poi', 'column' => array('name'))),
        new sfValidatorDoctrineUnique(array('model' => 'Poi', 'column' => array('slug'))),
      ))
    );

    $this->widgetSchema->setNameFormat('poi[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Poi';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['keyword_list']))
    {
      $this->setDefault('keyword_list', $this->object->Keyword->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveKeywordList($con);

    parent::doSave($con);
  }

  public function saveKeywordList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['keyword_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Keyword->getPrimaryKeys();
    $values = $this->getValue('keyword_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Keyword', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Keyword', array_values($link));
    }
  }

}
