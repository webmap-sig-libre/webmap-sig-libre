<?php

/**
 * KeywordPoi form base class.
 *
 * @method KeywordPoi getObject() Returns the current form's model object
 *
 * @package    webmap-sig-libre
 * @subpackage form
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseKeywordPoiForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'poi_id'     => new sfWidgetFormInputHidden(),
      'keyword_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'poi_id'     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('poi_id')), 'empty_value' => $this->getObject()->get('poi_id'), 'required' => false)),
      'keyword_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('keyword_id')), 'empty_value' => $this->getObject()->get('keyword_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('keyword_poi[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'KeywordPoi';
  }

}
