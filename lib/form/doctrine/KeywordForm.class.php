<?php

/**
 * Keyword form.
 *
 * @package    webmap-sig-libre
 * @subpackage form
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class KeywordForm extends BaseKeywordForm {

  public function configure() {
    // http://www.lexik.fr/blog/symfony/symfony/formulaires-i18n-et-ajout-de-langues-automatise-691
    /*$string = sfYaml::load(sfConfig::get('sf_apps_dir') . '/../config/app.yml');
    $this->languages = $string['cultures_enabled'];
    $keys = array_keys($this->languages);
    $this->embedI18n($keys);

    foreach ($this->languages as $key => $lang) {
      $this->widgetSchema->setLabel($key, $lang);
    }*/
    $this->embedI18n(array('fr', 'en'));
    $this->widgetSchema->setLabel('fr', 'French');
    $this->widgetSchema->setLabel('en', 'English');
    
    parent::configure();
    
    unset($this['pois_list']);

    /* Name moved to KeywordTranslationForm */

    /* Logo */
    $this->widgetSchema['logo'] = new sfWidgetFormInputFileEditable(array(
                'label' => 'Logo',
                'file_src' => '/uploads/logos/' . $this->getObject()->getLogo(),
                'is_image' => true
            ));

    $this->validatorSchema['logo'] = new sfValidatorFile(
                    array(
                        'required' => false,
                        'path' => sfConfig::get('sf_upload_dir') . '/logos',
                        'mime_types' => 'web_images'
                    ),
                    array(
                        'mime_types' => 'The logo must be a web image file (png, jpg or gif)'
                    )
    );

    $this->validatorSchema['logo_delete'] = new sfValidatorPass();

    /*$this->widgetSchema['en']['name'] = new sfWidgetFormInputText();
    $this->widgetSchema['fr']['name'] = new sfWidgetFormInputText();*/
  }

}
