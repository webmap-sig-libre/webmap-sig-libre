<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <?php use_helper('I18N') ?>
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <title><?php echo sfConfig::get('app_nom_app') . ' - Administration' . ' - ' . sfConfig::get('app_nom_ligue') ?></title>
    <link rel="shortcut icon" href="/images/favicon.png" />
    <?php use_stylesheet('admin.css') ?>
    <?php include_stylesheets() ?>

    <?php include_javascripts() ?>
  </head>
  <body>
    <div id="container">
      <div id="header">
        <div id="menu">
          <ul>
            <?php if ($sf_user->isAuthenticated()): ?>
            <li><?php echo link_to(image_tag('home', 'alt="' . __('Administration home') . '" title="' . __('Administration home') . '"'), '@homepage'); ?></li>
            <li><?php echo link_to(__('Point of Interest'), '@poi'); ?></li>
            <li><?php echo link_to(__('Keywords'), '@keyword'); ?></li>
            <li><?php echo link_to(__('Categories'), '@category'); ?></li>
            <li><?php echo link_to(__('Subcategories'), '@sub_category'); ?></li>
            <?php endif; ?>
            <?php // if ($sf_user->hasCredential('super_admin')): ?>
            <li><?php //echo link_to('Droits', '@droits'); ?></li>
            <?php // endif; ?>
            <?php if ($sf_user->isAuthenticated()): ?>
            <li><?php echo link_to(image_tag('disconnect', 'alt=Disconnect title="' . __('Disconnect') . '"'), '@sf_guard_signout'); ?></li>
            <?php  endif; ?>
          </ul>
        </div> <!-- menu -->
        <div id="logo">
          <a href="<?php echo url_for('@homepage'); ?>" title="Homepage"><img src="/images/logo.png" alt="logo" /></a>
        </div>
        <div id="defsite">
          <h1><?php echo sfConfig::get('app_nom_app') . ' - Administration' ?></h1>
        </div>        
      </div> <!-- header -->

      <div id="content">
        <?php echo $sf_content ?>
      </div> <!-- content -->

      <div id="footer">
        <div id="footmenu">
          <ul>
            <?php //if ($sf_user->hasCredential('gestion')): ?>
            <li><?php echo link_to(image_tag('home', 'alt="' . __('Administration home') . '" title="' . __('Administration home') . '"'), '@homepage'); ?></li>
            <?php //endif; ?>
            <?php //if ($sf_user->isAuthenticated()): ?>
            <li><?php echo link_to(image_tag('back', 'alt="' . ('Back to the map') . '" title="' . __('Back to the map') . '"'), '/'); ?></li>
            <?php //endif; ?>
          </ul>
        </div> <!-- footmenu -->
      </div><!-- footer -->
    </div> <!-- container -->
  </body>
</html>
