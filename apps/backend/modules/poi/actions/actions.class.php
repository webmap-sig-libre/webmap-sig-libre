<?php

require_once dirname(__FILE__).'/../lib/poiGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/poiGeneratorHelper.class.php';

/**
 * poi actions.
 *
 * @package    webmap-sig-libre
 * @subpackage poi
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class poiActions extends autoPoiActions
{
  public function executeListPublish(sfWebRequest $request) {
    $poi = $this->getRoute()->getObject();
    $poi->publish(TRUE);
    
    $this->getUser()->setFlash('notice', 'The selected poi is now published');
    
    $this->redirect('@poi');
  }
}
