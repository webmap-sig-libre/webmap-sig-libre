<?php

/**
 * keyword module configuration.
 *
 * @package    webmap-sig-libre
 * @subpackage keyword
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class keywordGeneratorConfiguration extends BaseKeywordGeneratorConfiguration
{
}
