<?php

require_once dirname(__FILE__).'/../lib/sub_categoryGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sub_categoryGeneratorHelper.class.php';

/**
 * sub_category actions.
 *
 * @package    webmap-sig-libre
 * @subpackage sub_category
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sub_categoryActions extends autoSub_categoryActions
{
}
