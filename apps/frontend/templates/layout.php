<html>
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <title>
      <?php echo sfConfig::get('app_nom_app') ?>
      <?php include_slot('title') ?>
    </title>
    <link rel="shortcut icon" href="/images/favicon.png" />
    <?php include_stylesheets() ?>
    <?php use_javascript('/js/openlayers/OpenLayers.js') ?>
    <?php use_javascript('/js/tiles.js') ?>
    <?php include_javascripts() ?>

  </head>
  <body <?php include_slot('init_js') ?>>
    <div id="container">
      <div id="header">
        <div id="menu">
          <ul>
            <li><?php echo link_to(__('Points of interest'), '@homepage'); ?></li>
          </ul>
        </div> <!-- menu -->
        <div id="logo">
          <a href="<?php echo url_for('@homepage'); ?>" title="Homepage"><img src="/images/logo.png" alt="logo" /></a>
        </div>
        <div id="defsite">
          <h1>
            <?php echo sfConfig::get('app_nom_app') ?>
            <?php include_slot('title') ?>
          </h1>
        </div> 
      </div> <!-- header -->

      <div id="content">
        <?php echo $sf_content ?>
      </div> <!-- content -->

      <div id="footer">
        <div id="footmenu">
          <ul>
            <li><?php echo link_to( __('Administration'), '/admin/')?></li>
            <li><a href="http://www.formation-lpi.com"><?php echo __('Conception') ?></a></li>
          </ul>
        </div> <!-- footmenu -->
      </div><!-- footer -->
    </div> <!-- container -->
  </body>
</html>
