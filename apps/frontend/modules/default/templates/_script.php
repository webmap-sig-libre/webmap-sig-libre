<script type="text/javascript">
  var map;
  
  function getDetails(poi_id) {
    $.get("/ajax/poi",
    { 'poi_id': poi_id },
    function(data) {
      $("#details").html(data);
    });
  };    
 
  
  function init() {
    /* Hauteur de la carte en fonction de la hauteur de l'écran - entête et pied */
    $("#map").height($(window).height() - 60);
    
    map = new OpenLayers.Map("map");
    OpenLayers.Lang.setCode("fr");
    //map.addLayer(new OpenLayers.Layer.OSM());
    map.addLayer(new OpenLayers.Layer.OSM.MapQuestOpen("Default"));
    // Centre de la carte    
    var centre = new OpenLayers.LonLat( <?php echo $coord['longitude'] . ',' . $coord['latitude'] ?>)
    .transform(
    new OpenLayers.Projection("EPSG:4326"),
    map.getProjectionObject()
  );

    var featurecollection = { "type":"FeatureCollection",
      "features": [
<?php
$nb = count($pois);
$i = 0;
foreach ($pois as $poi):++$i
  ?>
  <?php $lonlat = array($poi->getLongitude(), $poi->getLatitude()) ?>
            { "type": "Feature",
              "id": <?php echo $poi->getId() ?>,
              "geometry": {"type": "Point", "coordinates":<?php echo json_encode($lonlat) ?>},
              "properties": {
                "name": <?php echo json_encode($poi->getName()) ?>,
                "logo": <?php echo json_encode("/uploads/logos/" . $poi->getLogo()) ?> }              
              }<?php echo $nb == $i ? "" : ", \n" ?>
<?php endforeach; ?>
      ]}
      
    AutoSizeAnchored = OpenLayers.Class(OpenLayers.Popup.Anchored, {'autoSize': true});
    
    var style = new OpenLayers.Style({      
      externalGraphic: "${logo}",
      graphicWidth: 16,
      graphicHeight: 16,
      graphicYOffset: -8,
      graphicOpacity: 1,
      cursor: "pointer"      
    });
        
    var selectStyle = new OpenLayers.Style({
      graphicWidth: 20,
      graphicHeight: 20,
      graphicYOffset: -10,
      graphicOpacity: 1,
      cursor: "pointer"
    });
 
    var StyleMap = new OpenLayers.StyleMap({'default': style, 'select' : selectStyle}); 
     
    var layer = new OpenLayers.Layer.Vector("Pois",
    { styleMap: StyleMap,
      projection: map.displayProjection
    });
    var geojson_format = new OpenLayers.Format.GeoJSON({
      'internalProjection': new OpenLayers.Projection("EPSG:900913"),
      'externalProjection': new OpenLayers.Projection("EPSG:4326")
    });
    map.addLayer(layer);
    layer.addFeatures(geojson_format.read(featurecollection));
        
    var highlightControl= new OpenLayers.Control.SelectFeature(layer,
	{
        id: 'tooltip',  
	clickout: true,
	toggle: true,
	hover: true,
        highlightOnly: true,
        renderIntent: "temporary",
        eventListeners: { 
         'featurehighlighted': feature_hover,
         'featureunhighlighted': feature_out 
       }
    });
    
    var zoom=<?php echo $coord['zoom'] ?>;

    //map.addControl(new OpenLayers.Control.LayerSwitcher());   
    map.setCenter (centre, zoom);
    // Interaction; not needed for initial display.
    selectControl = new OpenLayers.Control.SelectFeature(layer);
    map.addControl(highlightControl);
    map.addControl(selectControl);
    highlightControl.activate();
    selectControl.activate();
    layer.events.on({
      'featureselected': onFeatureSelect,
      'featureunselected': onFeatureUnselect
    });    
  }

  // Needed only for interaction, not for the display.
  function feature_hover(event) {
    var feature = event.feature;
    var selectedFeature = feature;

    if(feature.popup != null){
            return;
        }
        //if there are other tooltips active, destroy them
        if(tooltipPopup != null){
            map.removePopup(tooltipPopup);
            tooltipPopup.destroy();
            if(lastFeature != null){
                delete lastFeature.popup;
                tooltipPopup = null;
            }
        }
        lastFeature = feature;
        var tooltipPopup = new OpenLayers.Popup("activetooltip",
                    feature.geometry.getBounds().getCenterLonLat(),
                    new OpenLayers.Size(),
                    "&nbsp;"+feature.attributes.name+"<br/><center><small><i>(<?php echo __('Click here for more details')?>)</i></small></center>", false );
        tooltipPopup.contentDiv.style.backgroundColor='fff';
        //tooltipPopup.closeDiv.style.backgroundColor='ffffcb';
        tooltipPopup.contentDiv.style.overflow='hidden';
        tooltipPopup.contentDiv.style.padding='3px';
        tooltipPopup.contentDiv.style.margin='0';
        tooltipPopup.closeOnMove = false;
        tooltipPopup.autoSize = true;
        feature.popup = tooltipPopup;
        map.addPopup(tooltipPopup);
    }
        
  function feature_out(event) {
    var feature = event.feature;
        if(feature != null && feature.popup != null){
            map.removePopup(feature.popup);
            feature.popup.destroy();
            delete feature.popup;
            tooltipPopup = null;
            lastFeature = null;
        }
  }
  
  function onPopupClose(evt) {
    // 'this' is the popup.
    var feature = this.feature;
    if (feature.layer) { // The feature is not destroyed
      selectControl.unselect(feature);
    } else { // After "moveend" or "refresh" events on POIs layer all 
      //     features have been destroyed by the Strategy.BBOX
      this.destroy();
    }
  }
  function onFeatureSelect(evt) {
    feature = evt.feature;    
    popup = new OpenLayers.Popup.FramedCloud("featurePopup",
    feature.geometry.getBounds().getCenterLonLat(),
    new OpenLayers.Size(300,300),
    "<h2>"+feature.attributes.name + "</h2>" 
      + "<div id=\"details\"></div>",
    null, true, onPopupClose);
    popup.autoSize = false;
    feature.popup = popup;
    popup.feature = feature;
    map.addPopup(popup, true);
    getDetails(feature.fid);
  }
  function onFeatureUnselect(evt) {
    feature = evt.feature;
    if (feature.popup) {
      popup.feature = null;
      map.removePopup(feature.popup);
      feature.popup.destroy();
      feature.popup = null;
    }
  }
  
</script>