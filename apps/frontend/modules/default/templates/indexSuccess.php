<?php slot('title', sprintf(' - ' .__('Points of interest'))) ?>
<?php slot('init_js', sprintf('onload="init()"')) ?>

<?php use_stylesheets_for_form($filter) ?>
<?php use_javascripts_for_form($filter) ?>
<?php use_javascript('/js/jquery/jquery-1.4.2.min.js') ?>

<script language="javascript">
$(document).ready(function(){
  $("#poi_filters_category_id").change( function() {
    $.post("/ajax/sub_category", 
    { 'category_id': $(this).val() },
    function(data){
      $("#poi_filters_sub_category_id").html(data);
    });
  });
});
</script>

<?php include_partial('default/script', array('pois' => $pois, 'coord' => $coord)) ?>

<div id="filter">
  <form action="<?php echo url_for('@pois_filter') ?>" method="post">
    <?php echo $filter->renderGlobalErrors() ?>
    <table>
      <tr><td><?php echo $filter['category_id']->renderLabel(null, array('class' => 'titre')) ?></td></tr>
      <tr><td><?php echo $filter['category_id']->render(array('onclick' => 'poi_filters_sub_category_id.value = "";')) ?></td></tr>
      <tr><td><?php echo $filter['sub_category_id']->renderLabel(null, array('class' => 'titre')) ?></td></tr>
      <tr><td><?php echo $filter['sub_category_id']->render(array('size' => 4)) ?></td></tr>
      <tr>
        <td>
          <?php echo link_to(__('Reset'), '@homepage') ?>
          <input type="submit" value="<?php echo __('Filter') ?>" />
        </td>
      </tr>
    </table>
    <?php echo $filter->renderHiddenFields() ?>
  </form>
</div><!-- filter -->
<div id="map"></div>