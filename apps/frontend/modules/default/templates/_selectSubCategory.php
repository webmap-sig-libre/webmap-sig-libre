<?php if($categories): ?>
  <?php foreach($categories as $category): ?>
    <?php foreach($category->getSubCategories() as $sub_category): ?>
    <option value="<?php echo $sub_category->getId() ?>"><?php echo $sub_category->__toString() ?></option>
    <?php endforeach; ?>
  <?php endforeach; ?>
<?php else : ?>
  <?php foreach($category->getSubCategories() as $sub_category): ?>
    <option value="<?php echo $sub_category->getId() ?>"><?php echo $sub_category->__toString() ?></option>
  <?php endforeach; ?>
<?php endif; ?>