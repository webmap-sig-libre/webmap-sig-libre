<?php use_helper('Thumb'); ?>
<?php echo showThumb($poi->getPhoto(), 'photos', array('width' => 100, 'height' => 100), 'scale', 'default.jpg'); ?>
  <div id="coord">
    <p><?php echo $poi->getAddress() ?></p>
    <p><?php echo $poi->getPostalCode() . " " . $poi->getCity() ?></p>
    <?php $phone = $poi->getPhone() ? $poi->getPhone() : "-" ?>
    <?php $email = $poi->getEmail() ? $poi->getEmail() : "-" ?>
    <?php $website = $poi->getWebsite() ? $poi->getWebsite() : "-" ?>
    <p><small><?php echo image_tag('/images/phone.png', 'alt="' . __('Phone:') . '"') . " " . $phone ?></small></p>
    <p><small><?php echo image_tag('/images/email.png', 'alt="Email:"') . " " . $email ?></small></p>
    <p><small><?php echo image_tag('/images/website.png', 'alt="Website:"') . " <a href=\"" . $website . "\">" . $website . "</a>" ?></small></p>
  </div>  

<div id="poi_details">
  <?php if ($poi->getDescription()) : ?>
    <hr/><h3><?php echo __('Description') ?></h3>
    <p><small><?php echo $poi->getDescription() ?> </small></p>
  <?php endif; ?>

  <hr/><h3><?php echo __('Services') ?></h3>
  <ul>
    <?php if (count($keywords) > 0): ?>
      <?php foreach ($keywords as $keyword) : ?>
        <li><?php echo $keyword ?></li>
      <?php endforeach ?>
    <?php else: ?>
      <li><i>-<?php echo __('none') ?> -</i></li>
    <?php endif; ?>
  </ul>
</div>