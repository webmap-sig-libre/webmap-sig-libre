<div id="texte">
  <h2>This page does not exist!</h2>

  <strong>Where to go?</strong>
  <ul>
    <li class="sfTLinkMessage"><a href="javascript:history.go(-1)">Back to previous page</a></li>    
    <li><?php echo link_to('Homepage', '@homepage'); ?></li>
    <li><a href="/backend.php/admin">Go to the administration</a></li>    
  </ul>
</div>