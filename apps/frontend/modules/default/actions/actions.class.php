<?php

/**
 * default actions.
 *
 * @package    webmap-sig-libre
 * @subpackage default
 * @author     Éric Deschamps <erdesc@formation-lpi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class defaultActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request) {
    if (!$request->getParameter('sf_culture')) {
      if($this->getUser()->isFirstRequest()) {
        $culture = $request->getPreferredCulture(array('en', 'fr'));
        $this->getUser()->setCulture($culture);
        $this->getUser()->isFirstRequest(false);
      }
      else {
        $culture = $this->getUser()->getCulture();
      }
      $this->redirect('@localized_homepage');
    }
    $this->filter = new FrontPoiFormFilter($this->getFilter());
    $this->pois = Doctrine_Core::getTable('Poi')->getFrontPublishedPois();
    $this->coord = $this->getCoordCarte();
  }
  
  public function executeFilter(sfWebRequest $request) {
    $this->filter = new FrontPoiFormFilter($this->getFilter());
    $this->filter->bind($request->getParameter($this->filter->getName()));
    if ($this->filter->isValid()) {
      $this->pois = $this->filter->getQuery()->execute();
    }
    $this->coord = $this->getCoordCarte();
    $this->setTemplate('index');
  }
  
  public function executeShowPoi(sfWebRequest $request) {
    $poi = Doctrine::getTable('Poi')->find($request->getParameter('poi_id'));
    $keywords = Doctrine::getTable('Poi')->getKeywords($poi->getId());   
    
    return $this->renderPartial('default/showPoi', array('poi' => $poi, 'keywords' => $keywords));
  }

  /* to test the executeShowDetails query*/

  public function executeDetails(sfWebRequest $request) {  
    $this->poi = Doctrine::getTable('Poi')->find(3);
    $this->keywords = Doctrine::getTable('Poi')->getKeywords($this->poi->getId());
  }   

  public function executeAjaxSubCategory(sfWebRequest $request) {
    if ($request->getParameter('category_id')) {
      $category = Doctrine::getTable('Category')->findOneById($request->getParameter('category_id'));
    } else {
      $categories = Doctrine::getTable('Category')->findAll();
    }

    return $this->renderPartial('default/selectSubCategory', array('category' => $category, 'categories' => $categories));
  }

  protected function getFilter() {
    return $this->getUser()->getAttribute('filtre', array());
  }

  protected function setFilter(array $filter) {
    return $this->getUser()->setAttribute('filtre', $filter);
  }

  protected function buildQuery($q) {
    if (!isset($this->filter))
      $this->filter = $this->getFilter();

    $this->filter->setQuery($q);

    $query = $this->filter->buildQuery($this->getFilter());
    return $query->execute();
  }

  protected function getCoordCarte() {
    $coord = array('zoom' => sfConfig::get('app_default_zoom'), 'longitude' => sfConfig::get('app_default_lon'), 'latitude' => sfConfig::get('app_default_lat'));
    return $coord;
  }
  
  public function executeError404()
{
}
  
}
