  var map;
  function init() {
    map = new OpenLayers.Map("map");
    OpenLayers.Lang.setCode("fr");
    map.addLayer(new OpenLayers.Layer.OSM());
    // Centre de la carte    
    var centre = new OpenLayers.LonLat( <?php echo sfConfig::get('app_default_lon') . ', ' . sfConfig::get('app_default_lat') ?>)
    .transform(
    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
    map.getProjectionObject() // to Spherical Mercator Projection
  );

    var zoom=<?php echo sfConfig::get('app_default_zoom') ?> 
      
    AutoSizeAnchored = OpenLayers.Class(OpenLayers.Popup.Anchored, {'autoSize': true});
    
    var style = new OpenLayers.Style({
                /*strokeColor: "green", 
                strokeWidth: 1, 
                strokeOpacity: 0.5,
                fillColor : "black",*/
                pointRadius : 8,
                externalGraphic: '${icon}'
        });
        
    var selectStyle = new OpenLayers.Style({
      pointRadius: 10
    });
 
    var StyleMap = new OpenLayers.StyleMap({'default': style, 'select' : selectStyle}); 

    // couche representant les clubs à partir de l'export en texte'
    /*
    var layer = new OpenLayers.Layer.Text( "Clubs",
    { location:"/index.php/clubstotxt",
      projection: map.displayProjection
    });*/
     
    var layer = new OpenLayers.Layer.GML("Clubs", "/index.php/clubstojson", {
      format: OpenLayers.Format.GeoJSON,
      styleMap: StyleMap
    });
    map.addLayer(layer);
    //layer.addFeatures(geojson_format.read(featurecollection));

    //map.addControl(new OpenLayers.Control.LayerSwitcher());
    map.setCenter (centre, zoom);
    
    // Interaction; not needed for initial display.
    selectControl = new OpenLayers.Control.SelectFeature(layer);
    map.addControl(selectControl);
    selectControl.activate();
    layer.events.on({
      'featureselected': onFeatureSelect,
      'featureunselected': onFeatureUnselect
    });
  }
  
  // Needed only for interaction, not for the display.
  function onPopupClose(evt) {
    // 'this' is the popup.
    var feature = this.feature;
    if (feature.layer) { // The feature is not destroyed
      selectControl.unselect(feature);
    } else { // After "moveend" or "refresh" events on POIs layer all 
      //     features have been destroyed by the Strategy.BBOX
      this.destroy();
    }
  }
  function onFeatureSelect(evt) {
    feature = evt.feature;
    popup = new OpenLayers.Popup.FramedCloud("featurePopup",
    feature.geometry.getBounds().getCenterLonLat(),
    new OpenLayers.Size(100,100),
    "<h2>"+feature.attributes.nom + "</h2>" +
      "<i>(" + feature.attributes.ville + "</i>)<hr/>" +
      "<strong>Contacts</strong> <br/><small><img src=\"/images/email.png\" alt=\"courriel :\" /><a href=\"mailto:" +
      feature.attributes.mail + "\"> " + feature.attributes.mail + "</a></small><br/>" +
      "<small><small><img src=\"/images/phone.png\" alt=\"tél :\" /> " + feature.attributes.telephone + "</small><br/>"  ,
    null, true, onPopupClose);
    feature.popup = popup;
    popup.feature = feature;
    map.addPopup(popup, true);
  }
  function onFeatureUnselect(evt) {
    feature = evt.feature;
    if (feature.popup) {
      popup.feature = null;
      map.removePopup(feature.popup);
      feature.popup.destroy();
      feature.popup = null;
    }
  }
  